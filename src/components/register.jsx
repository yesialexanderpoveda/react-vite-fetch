/* import "../../styles/landingpage/register.css"; */
import { FormattedMessage } from "react-intl";
import { useForm } from "react-hook-form"
export const Register = () => {

    const { register, formState: { errors }, handleSubmit } = useForm();
    const onSubmit = data => {
        console.log(data);
    }

    return (
        <div className="register">
            <form onSubmit={handleSubmit(onSubmit)}>
                <h3>Challenge Alkemy</h3>
                <label>
                    <p>Nombre</p>
                    <input type="text" {...register("name", { required: true })} />
                    <br/>
                    {errors.name?.type === "required" &&
                        <FormattedMessage
                            id="reg.namev"
                            defaultMessage="Debe ingresar el nombre"
                        />
                    }
                </label>
                <label>
                    <p>Correo</p>
                    <input type="text" {...register("email", {required: true, pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/})} />
                    <br/>
                    {(errors.email?.type  === "pattern" || errors.email?.type === "required" ) && <FormattedMessage
                id="footer.e-mailv"
                defaultMessage="Correo invalido"
                />
          }
                </label>
                <label>
                    <p>Contraseña</p>
                    <input type="password" {...register("password", { required: true })} />
                     <br/>
                     {errors.password?.type === "required" &&
                        <FormattedMessage
                            id="reg.password"
                            defaultMessage="Debe ingresar el nombre"
                        />
                      }

                </label>
                <div>
                    <br />
                    <button type="submit" value="submit" >Registrar</button>
                    
                    
                </div >
            </form>
        </div>
    )
}