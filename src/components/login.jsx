import { useState } from "react";
import { useForm } from "react-hook-form"
import { Link, useNavigate} from "react-router-dom";
import { Servicelog } from "../services/servicelog";


export const Login = () => {

    const navigate = useNavigate();
    const { register, formState: { errors }, handleSubmit } = useForm();
    
    const  onSubmit = async prop => {
        
      fetch('http://localhost:8080/v1/log', {
        method: 'POST', // or 'PUT'
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(prop),
      })
        .then((response) => response.json())
        .then((data) => {
          if(data){
            console.log('Success:', data);   
            Servicelog(data);
            navigate("/profile");
          }
          
        })
        .catch((error) => {
          console.error('Error:', error);
          alert("el usuario no exite")
        });
      
        
        
        
    }
    
    return(
       
        <div className="login">
        <form onSubmit={handleSubmit(onSubmit)}>
        <h3>Challenge Alkemy</h3>
        <label>
          <p>Correo</p>
          <input type="text" {...register("email", { required: true })}/>
        </label>
        <label>
          <p>Contraseña</p>
          <input type="password" {...register("password", { required: true })} />
        </label>
        <div>
        <br/>
          <button type="submit" value="submit">Ingresar</button>
          <br/>
          <p><Link to="/register" className="li">crear una nueva cuenta</Link> </p>
        </div>
      </form>
      
      </div>  
    );
}