import {Link, Outlet} from 'react-router-dom'
import {Register} from "../components/register";
import {Login} from "../components/login";

const Landing = () => {
    return (
        <>
        <nav>
        <Link to="register">Profile</Link>
        <Link to="login">Account</Link>
      </nav>

      <Outlet />
      </>
    );

}

export default Landing;