
export async function Total(prop) {

    console.log(prop.token)
    // first fetch 
    fetch('http://localhost:8080/v1/salary', {
        method: 'GET', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${prop.token}`
        }
    }).then((response) => response.json())
        .then((data) => {
            if (data) {
                console.log('Success:', data[0]);
            }
        })
    // second fetch 

        fetch('http://localhost:8080/v1/input-total', {
            method: 'GET', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${prop.token}`
            }
        }).then((response) => response.json())
            .then((data) => {
                if (data) {
                    console.log('Success:', data[0]);
                }
            })

        // Other fetch 
            fetch('http://localhost:8080/v1/output-total', {
                method: 'GET', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${prop.token}`
                }
            }).then((response) => response.json())
                .then((data) => {
                    if (data) {
                        console.log('Success:', data[0]);
                    }
                })
}
