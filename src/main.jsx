import React from 'react'
import ReactDOM from 'react-dom/client'
import {App} from './App'
import {IntlProvider} from "react-intl";
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
     <IntlProvider locale='es-CO' defaultLocale="es-CO">
    <App />
    </IntlProvider>
  </React.StrictMode>
)
