import React, { useState, useEffect } from 'react';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import { NavbarLanding, NavbarProfile } from './components/navbar';
import Protected from './protected';
import { Servicelog } from './services/servicelog';
import Profile from './pages/profile';
import Landing from './pages/landing';
import { NoMatch } from './pages/match';
import { Login } from './components/login';
import { Register } from './components/register';


export const App = () => {


  const [isLoggedIn, setisLoggedIn] = useState(null)

  return (
    <BrowserRouter>
      <div>
        {isLoggedIn ? (
          <nav>

          </nav>


        ) : (
          <nav>

            <Servicelog value={setisLoggedIn(true)} />
            <NavbarLanding />

          </nav>

        )}

        <Routes>
          <Route path="/" element={<Landing />} >
            <Route path="login" element={<Login />} />
            <Route path="register" element={< Register />} />
            <Route index element={<Login />} />
          </Route>
          <Route path='/profile' element={
            <Protected isLoggedIn={isLoggedIn}>
              <Profile />
            </Protected>
          }
          >
            <Route index element={<Profile />} />
          </Route>

          <Route path="*" element={<NoMatch />} />
        </Routes>

      </div>
    </BrowserRouter>
  );
}


